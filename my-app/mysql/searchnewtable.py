import re  
import os  
import mysql.connector  
from mysql.connector import Error  
from datetime import date, datetime, timedelta  

# 定义多个数据库实例的连接配置  
database_configs = [  
    {  
        'name': '001prd-价格',
        'host': 'pc-uf6k52082rjj06r18.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',
        'port': 3306 
    },  
    {  
        'name': '002prd-数据中心',
        'host': 'pc-uf62x11h8eo046834.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '003prd-综合',
        'host': 'pc-uf6zfz353vss97j07.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '004prd-老国内',
        'host': 'zgl95mgam8xrk97dnvu5-rw4rm.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '005prd-重构',
        'host': 'w7hnvlzoasskmgg42t08-rw4rm.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '006prd-三方库',
        'host': 'mr-s2jg2dgdr9nn46f4d8.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '007prd-国际',
        'host': 'a5bux63jx7o.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',
        'port': 5998  
    }  
]  

# 匹配日期表格模式
date_pattern = re.compile(r'\d{8}|\d{4}_\d{2}') 

# 计算的开始日期和结束日期
startDate = '2024-10-08'
endDate = '2024-10-12'

def execute_query_on_all_databases():  
    for config in database_configs:  
        # 1 移除原来的文件记录
        file_path = config['name'] + '(' + startDate + '~' + endDate + ')' + '.txt'
        if os.path.exists(file_path):  
            os.remove(file_path)  
        os.open(file_path, os.O_CREAT)
        
        try:  
            # 建立数据库连接  
            connection = mysql.connector.connect(  
                host=config['host'],  
                database=config['database'],  
                user=config['user'],  
                password=config['password'],
                port=config['port']  
            )  
            
            if connection.is_connected():  
                print(f"Connected to the database: {config['name']} successfully.............................")  

                # 创建游标对象  
                cursor = connection.cursor()  
                
                # 定义SQL查询  
                query = f"""  
                SELECT DATE(CREATE_TIME), TABLE_SCHEMA, TABLE_NAME   
                FROM information_schema.TABLES   
                WHERE TABLE_SCHEMA NOT IN ('information_schema', 'xxl_job', 'tonglian', 'cs_gs_ds', 'push_message')   
                AND CREATE_TIME >= '{startDate}' AND  CREATE_TIME <= '{endDate}';
                """  
                # 执行SQL查询  
                cursor.execute(query)  
                            # 获取查询结果  
                results = cursor.fetchall()

                # 输出结果  
                for row in results:  
                    if date_pattern.search(row[2]):  
                        continue
                    content = f"Date: {row[0]}, Schema: {row[1]}, Table: {row[2]}"
                    print(content)  
                    # 打开文件进行写入（如果文件不存在，将自动创建）， 
                    with open(file_path, 'a', encoding='utf-8') as file:  
                        file.write(content + '\n')  
        except Error as e:  
            print(f"Error connecting to database {config['database']} on host {config['host']}: {e}")  
        
        finally:  
            # 关闭数据库连接  
            if connection.is_connected():  
                cursor.close()  
                connection.close()  
                print(f"Connected to the database: {config['name']} close.............................")  


# // 执行查询
execute_query_on_all_databases()

# 定义定时任务  
# schedule.every(1).hours.do(execute_query_on_all_databases)  

# # 运行定时任务  
# while True:  
#     schedule.run_pending()  
#     time.sleep(1)