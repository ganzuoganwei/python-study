import mysql.connector  
from mysql.connector import Error  

# 定义多个数据库实例的连接配置  
database_configs = [  
    {  
        'name': '001prd-价格',
        'host': 'pc-uf6k52082rjj06r18.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',
        'port': 3306 
    },  
    {  
        'name': '002prd-数据中心',
        'host': 'pc-uf62x11h8eo046834.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '003prd-综合',
        'host': 'pc-uf6zfz353vss97j07.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '004prd-老国内',
        'host': 'zgl95mgam8xrk97dnvu5-rw4rm.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '005prd-重构',
        'host': 'w7hnvlzoasskmgg42t08-rw4rm.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '006prd-三方库',
        'host': 'mr-s2jg2dgdr9nn46f4d8.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',  
        'port': 3306 
    },  
    {  
        'name': '007prd-国际',
        'host': 'a5bux63jx7o.rwlb.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',
        'port': 5998  
    },
    {  
        'name': '008prd-价格-old',
        'host': 'rm-uf6c79hpo95t735zwjo.mysql.rds.aliyuncs.com',  
        'database': 'information_schema',  
        'user': 'ops_readonly',  
        'password': 'tYYSjAucieMQKQAh#3',
        'port': 3306  
    }    
]  

query = f"""  
    SELECT TABLE_SCHEMA, TABLE_NAME
    FROM information_schema.TABLES
    WHERE TABLE_NAME like '%bak%' or TABLE_NAME LIKE '%copy%';
    """  

def find_tables_with_patterns(configs):  
    for config in configs:  
        try:  
            connection = mysql.connector.connect(  
                host=config['host'],  
                database=config['database'],  
                user=config['user'],  
                password=config['password'],  
                port=config['port']  
            )  
  
            if connection.is_connected():  
                cursor = connection.cursor()  
                cursor.execute(query)  
                
                tables = cursor.fetchall()  
                filename = f"copy_table_{config['name']}.txt"  
                
                with open(filename, 'w', encoding='utf-8') as file:  
                    file.write(f"Database '{config['name']}' has the following tables with 'copy' or 'bak':\n")  
                    for table in tables:  
                        file.write(f"schema: {table[0]} table: {table[1]} \n")  
                print(f"Results written to {filename}")  
                
        except Error as e:  
            print(f"Error connecting to database '{config['name']}': {e}")  
        finally:  
            if connection.is_connected():  
                cursor.close()  
                connection.close()  

find_tables_with_patterns(database_configs)  