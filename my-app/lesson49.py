# 定义函数
def foo():
    print(bar())
    return 1
# 定义函数
def bar():
    return 2
# 函数的调用方式
print(foo())
# lambda 表达式的函数定义方式
a = lambda x: x + 1
print(a(2))