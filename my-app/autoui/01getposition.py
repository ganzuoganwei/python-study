import pyautogui
import time

print("按下 Esc 键退出程序...")

while True:    

    # 等待一段时间，以便用户有机会看到坐标
    time.sleep(3)  # 等待1秒
    x, y = pyautogui.position()
    print(f"当前鼠标位置: x={x}, y={y}")

