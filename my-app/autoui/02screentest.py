import pyautogui
import time

# try:
#     # 等待一段时间，以便你可以将鼠标移动到微信聊天列表的位置
#     time.sleep(2)

#     # 移动到微信聊天列表的大致位置，这里需要你根据实际情况调整坐标
#     pyautogui.moveTo(2798, 210)

#     # 按住鼠标左键
#     pyautogui.mouseDown()

#     # 向下拖动，这里以拖动300像素为例，你可以根据需要调整这个值
#     pyautogui.drag(0, 300, duration=2)

#     # 释放鼠标左键
#     pyautogui.mouseUp()
# except Exception as e:
#     print(f"发生异常：{e}")
#     pyautogui.mouseUp()  # 确保在异常情况下释放鼠标左键
# import pyautogui

# 将鼠标移动到屏幕的顶部
# pyautogui.moveTo(2591, 218)

# 按住鼠标左键
# pyautogui.mouseDown()

# # 向下拖动到屏幕的底部
# try:
#     print("开始拖动")
#     pyautogui.mouseDown()
#     print('Mouse down')  
#     pyautogui.moveRel(0, 500, duration=1)
#     print('Mouse moveRel')  
#     pyautogui.mouseUp()
#     print("拖动完成")
# except Exception as e:
#     print(f"发生异常：{e}")

# # 释放鼠标左键
# # pyautogui.mouseUp()

# try:  
#     print("Attempting to click...")  
#     pyautogui.click()  
#     print("Click executed successfully.")  
# except Exception as e:  
#     print("An error occurred:", e) 


# try:  
#     while True:  
#         # 获取当前鼠标位置  
#         x, y = pyautogui.position()  
#         print(f"当前鼠标位置: x={x}, y={y}")  
#         time.sleep(1)  # 每秒更新���次位置  
# except KeyboardInterrupt:  
#     print("\n位置获取已终止")  

# 截图
# 指定要截取的区域 (left, top, width, height)  
region = (1291, 347, 412, 740)  # 示例区域  

# 截取指定区域  
screenshot = pyautogui.screenshot(region=region)  

# 保存截图,截图保存001的位置。  
screenshot.save('screenshot.png')  
# 截图002的位置
print("截图已保存")  