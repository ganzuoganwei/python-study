from pathlib import Path
from openai import OpenAI
 
client = OpenAI(
    api_key = "sk-e4JFgK8yblHVZr4XnkzhNUbPIPJhoR0497Pz9Z6yoAyPOA69",
    base_url = "https://api.moonshot.cn/v1",
)
 
# user_3.png 是一个示例文件, 我们支持 pdf, doc 以及图片等格式, 对于图片和 pdf 文件，提供 ocr 相关能力
file_object = client.files.create(file=Path("user/user_3.png"), purpose="file-extract")
 
# 获取结果
# file_content = client.files.retrieve_content(file_id=file_object.id)
# 注意，之前 retrieve_content api 在最新版本标记了 warning, 可以用下面这行代替
# 如果是旧版本，可以用 retrieve_content
file_content = client.files.content(file_id=file_object.id).text
 
# 把它放进请求中
messages = [
    {
        "role": "system",
        "content": "你是 Kimi，由 Moonshot AI 提供的人工智能助手，你更擅长图片OCR结构化识别文字",
    },
    {
        "role": "system",
        "content": file_content,
    },
    {"role": "user", "content": "这些图片中包含用户信息，用户信息的格式是 姓名_机构名称"},
]
 
# 然后调用 chat-completion, 获取 Kimi 的回答
completion = client.chat.completions.create(
  model="moonshot-v1-32k",
  messages=messages,
  temperature=0.3,
)
 
print(completion.choices[0].message)