import time  
import cv2  
import pyautogui  
import numpy as np  
import os  
import re

# 定义截图函数  
def capture_screenshot(save_path='screenshot.png'):  
    # 指定要截取的区域 (left, top, width, height)  
    region = (1291, 347, 412, 740)  # 截图区域  
    screenshot = pyautogui.screenshot(region=region)  
    screenshot.save('user' + '/' + save_path)  
    print(f"截图已保存到 {save_path}")  

# 定义函数来搜索和点击模板  
def search_and_click_template(screenshot, template_path, isClick, threshold=0.9):  
    template = cv2.imread(template_path, cv2.IMREAD_GRAYSCALE)  
    # Check if the template was successfully loaded  
    if template is None:  
        print(f"Error: Could not read the template image from {template_path}")  
        return False  
    
    template_h, template_w = template.shape[:2]  

    result = cv2.matchTemplate(screenshot, template, cv2.TM_CCOEFF_NORMED)  
    locations = np.where(result >= threshold)  
    time.sleep(1)  
    if locations[0].size > 0:  
        for point in zip(*locations[::-1]):  # 转换坐标顺序  
            x, y = point  
            if isClick:  
                pyautogui.moveTo(x-10, y)  
                pyautogui.leftClick()  
                time.sleep(5)  
            return True  
    return False  

def extract_number(filename):  
    # Use regular expression to find numbers in the filename  
    match = re.match(r"(\d+)_", filename)  
    return int(match.group(1)) if match else 0  

# 遍历 category 文件夹中的所有模板图像  
category_folder = 'category-detail'  
screenshot = pyautogui.screenshot()  
screenshot = cv2.cvtColor(np.array(screenshot), cv2.COLOR_RGB2GRAY)  
# List the files in the directory  
file_list = os.listdir(category_folder)  

# Sort the files based on the extracted number  
templates = sorted(file_list, key=extract_number)  

previous_template_name = None  
index = 0
i = 2950  
for template_name in templates:  
    num = extract_number(template_name)
    if extract_number(template_name) < 1527:
        continue
    if index != 0 :
        previous_template_name = templates[index-1]
    time.sleep(2)  
    screenshot = pyautogui.screenshot()  
    screenshot = cv2.cvtColor(np.array(screenshot), cv2.COLOR_RGB2GRAY)
    ## 本次
    template_path = os.path.join(category_folder, template_name)    
    found = search_and_click_template(screenshot, template_path,True)    
    if found:
        print(template_name + 'found')
    else: print(template_name + 'not found')
    ## 下次
    next_index = templates.index(template_name) + 1
    next_template_path = os.path.join(category_folder, templates[next_index])
    next_found = search_and_click_template(screenshot, next_template_path, False)
    if next_found:
        print(templates[next_index] + 'next found but not click')
    else: print(templates[next_index] + 'next not found')

    # ## 找到了，保存图片，进入下一次循环
    # if found and not next_found:
    #     # 截取图像  
    #     screenshot_name = f"user_{i}.png"  
    #     i += 1
    #     capture_screenshot(screenshot_name)  
    # else: print('not found')
    # 没找到，说明还在上一个分类中， 继续捕捉图片
    while not found and not next_found:  
        # 截取图像  
        # time.sleep(1)  
        screenshot_name = f"user_{i}.png"
        capture_screenshot(screenshot_name)  
        # 滚动页面  
        pyautogui.scroll(-730)  
        i += 1  
        time.sleep(2)  
        screenshot = pyautogui.screenshot()  
        screenshot = cv2.cvtColor(np.array(screenshot), cv2.COLOR_RGB2GRAY)
        found = search_and_click_template(screenshot, template_path,True)  
        next_found = search_and_click_template(screenshot, next_template_path, False)
        time.sleep(2)  
    index = index + 1
    

