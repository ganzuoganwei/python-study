from pywinauto import Desktop  

# 获取当前运行的窗口  
windows = Desktop(backend='uia').windows()  # 'uia' 是现代 Windows 应用支持的后端  
for win in windows:  
    print(f"窗口句柄: {win.handle}, 窗口标题: {win.window_text()}") 

import subprocess  

subprocess.run([  
    "C:/Users/xiong/AppData/Local/Programs/Python/Python313/python.exe",  
    "d:/my-code/dm-data/python-study/my-app/autoui/test001.py"  
])  