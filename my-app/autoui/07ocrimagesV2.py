import cv2
import pytesseract
from PIL import Image
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

# 假设你已经有了一个图像文件路径
image_path = 'user/user_3.png'

# 读取图像
image = cv2.imread(image_path)

# 转换为灰度图像
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# 应用高斯模糊去噪
blurred = cv2.GaussianBlur(gray, (5, 5), 0)

# 使用Otsu's方法自动确定阈值进行二值化
_, binary = cv2.threshold(blurred, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

# 寻找轮廓
contours, _ = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

# 创建一个空白图像用于绘制轮廓
contour_image = cv2.cvtColor(binary, cv2.COLOR_GRAY2BGR)

# 绘制轮廓
cv2.drawContours(contour_image, contours, -1, (0, 255, 0), 2)

# 显示预处理后的图像
cv2.imshow('Preprocessed Image', contour_image)
cv2.waitKey(0)
cv2.destroyAllWindows()

# 将预处理后的图像转换为PIL格式以供pytesseract使用
pil_image = Image.fromarray(cv2.cvtColor(contour_image, cv2.COLOR_BGR2RGB))

# 使用pytesseract进行OCR
custom_config = r'--oem 3 --psm 6'
text = pytesseract.image_to_string(pil_image, config=custom_config)

# 打印识别的文本
print(text)