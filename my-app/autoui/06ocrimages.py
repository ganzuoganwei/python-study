from rapidocr_onnxruntime import RapidOCR  
from PIL import Image  
import os
import re

# Usage example with dummy coordinates  
# image_path = 'categoryimage.png'  
# output_path = 'cropped_image.png'  
# coords = (25.0, 9.0, 81.0, 22.0)  # Example coordinates for cropping  

# Crop the image using the function  
# crop_image(image_path, coords, output_path)  

def process_ocr_results(result):  
    for a in result:  
        if '/' in a[1]:  
            print(a[1] + '人员总数')  
            continue  
        # Print output (for debugging)  
        print(a[1])  


def ocr_with_rapidocr(image_path):  
    try:  
        ocr = RapidOCR()  

        results = ocr(image_path)  

        if not results:  
            print("No text detected.")  
            return  
        process_ocr_results(results[0])
    except Exception as e:  
        print(f"An error occurred: {e}")  


def extract_number(filename):
    # Use regex to find the number in the filename
    match = re.search(r'(\d+)', filename)
    return int(match.group(1)) if match else float('inf')

# 遍历 category 文件夹中的所有模板图像  
user_category = 'user'  
file_list = os.listdir(user_category)  
# Sort the files based on the extracted number  
templates = sorted(file_list, key=extract_number)  

for template_name in templates:  
    file_path = os.path.join(user_category, template_name)
    ocr_with_rapidocr(file_path)
    print(template_name + 'end')