from rapidocr_onnxruntime import RapidOCR  
from PIL import Image  
import os
import re

# Define a function to crop and save the image  
def crop_image(image_path, coords, output_path):  
    """  
    Crops an image to the defined bounding box.  

    :param image_path: Path to the input image.  
    :param coords: Tuple of (left, upper, right, lower) to define the cropping area.  
    :param output_path: Path to save the cropped image.  
    """  
    try:  
        # Open an image file  
        with Image.open(image_path) as img:  
            # Crop image to the coordinates  
            cropped_img = img.crop(coords)  
            
            # Save the cropped image to the given output path  
            cropped_img.save(output_path)  
            
            print(f"Cropped image saved as {output_path}")  
    except Exception as e:  
        print(f"An error occurred: {e}")  

# Usage example with dummy coordinates  
# image_path = 'categoryimage.png'  
# output_path = 'cropped_image.png'  
# coords = (25.0, 9.0, 81.0, 22.0)  # Example coordinates for cropping  

# Crop the image using the function  
# crop_image(image_path, coords, output_path)  

i = 1  
def process_ocr_results(result,image_path):  
    global i
    for a in result:  
        if '/' in a[1]:  
            print(a[1] + '人员总数')  
            continue  
        
        # Extract coordinates for cropping  
        coords = a[0][0][0] - 10, a[0][0][1] - 5, a[0][2][0] + 5, a[0][2][1] + 5  
        
        # Properly construct the output path  
        output_directory = 'category-detail'  
        if not os.path.exists(output_directory):  
            os.makedirs(output_directory)  
        
        output_path = os.path.join(output_directory, f'{i}_test.png')  
        
        # Print output (for debugging)  
        print(a[0], a[1], a[2])  
        print(output_path)  
        crop_image(image_path, coords, output_path)
        # Increase counter  
        i += 1  


def ocr_with_rapidocr(image_path):  
    try:  
        ocr = RapidOCR()  

        results = ocr(image_path)  

        if not results:  
            print("No text detected.")  
            return  
        process_ocr_results(results[0], image_path)
    except Exception as e:  
        print(f"An error occurred: {e}")  


def extract_number(filename):  
    # Use regex to find the number in the format .pngN  
    match = re.search(r'\.png(\d+)', filename)  
    return int(match.group(1)) if match else float('inf')  

# 遍历 category 文件夹中的所有模板图像  
category_folder = 'my-images'  
file_list = os.listdir(category_folder)  
# Sort the files based on the extracted number  
templates = sorted(file_list, key=extract_number)  

for template_name in templates:  
    file_path = os.path.join(category_folder, template_name)
    ocr_with_rapidocr(file_path)