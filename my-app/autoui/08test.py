import time
from pywinauto import Application
from pywinauto.findwindows import ElementNotFoundError
from PIL import ImageGrab

def capture_control_screenshot(window_title, control_type, save_path):
    """
    自动识别指定窗口内的控件并截图
    参数:
        window_title: 目标窗口标题(支持正则表达式)
        control_type: 要截图的控件类型 ('Button', 'Edit' 等)
        save_path: 截图保存路径
    """
    try:
        # 连接到目标窗口
        app = Application(backend='uia').connect(title_re=window_title)
        main_window = app.window(title_re=window_title)

        # 遍历查找所有指定类型的控件
        controls = main_window.descendants(control_type=control_type)
        print(f"找到 {len(controls)} 个 {control_type} 控件")

        # 逐个控件截图
        for idx, ctrl in enumerate(controls):
            # 获取控件的屏幕坐标
            rect = ctrl.rectangle()
            
            # 截图并保存（PIL像素坐标系需要调整）
            screenshot = ImageGrab.grab((
                rect.left,   # left
                rect.top,    # top
                rect.right,  # right
                rect.bottom  # bottom
            ))
            
            filename = f"{save_path}/{control_type}_{idx}_{int(time.time())}.png"
            screenshot.save(filename)
            print(f"已保存截图: {filename}")

    except ElementNotFoundError:
        print(f"未找到标题包含 '{window_title}' 的窗口")

# 使用示例：截取记事本中所有按钮
capture_control_screenshot(".*消息.*", "Button", "./screenshots")
