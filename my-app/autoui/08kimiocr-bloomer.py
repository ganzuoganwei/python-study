from pathlib import Path
from openai import OpenAI
import json
import os
import pandas as pd
from datetime import datetime
import time
from concurrent.futures import ThreadPoolExecutor, as_completed
 
client = OpenAI(
    api_key = "sk-sWYdFa1LS4pYzLfuRp5crrRTZ72fEU4QySAIIxXDlv1Odhs4",
    # api_key = "sk-e4JFgK8yblHVZr4XnkzhNUbPIPJhoR0497Pz9Z6yoAyPOA69",
    base_url = "https://api.moonshot.cn/v1",
)
 
 
def safe_float(value):
    """安全地转换为浮点数，失败返回None"""
    if value is None:
        return None
    try:
        float_val = float(str(value).replace(',', ''))
        return float_val
    except (ValueError, TypeError):
        return None

def safe_int(value):
    """安全地转换为整数，失败返回None"""
    if value is None:
        return None
    try:
        # 移除可能的逗号和小数点
        clean_value = str(value).replace(',', '').split('.')[0]
        return int(clean_value)
    except (ValueError, TypeError):
        return None

def safe_string(value):
    """安全地转换为字符串，失败返回None"""
    if value is None:
        return None
    try:
        return str(value).strip() or None
    except (ValueError, TypeError):
        return None

def standardize_data(item):
    """标准化数据格式，统一处理空值情况"""
    if not isinstance(item, dict):
        return create_empty_data()

    try:
        return {
            "代码": safe_string(item.get("代码")),
            "最新价": safe_float(item.get("最新价")),
            "量": safe_int(item.get("量")),
            "未平仓合约": safe_int(item.get("未平仓合约")),
            "合约单位": safe_string(item.get("合约单位")),
            "时间": safe_string(item.get("时间")),
            "涨跌幅": safe_string(item.get("涨跌幅")),
            "买价": safe_float(item.get("买价")),
            "卖价": safe_float(item.get("卖价"))
        }
    except Exception as e:
        print(f"处理数据项时出错: {e}")
        return create_empty_data()

def create_empty_data():
    """创建空数据结构"""
    return {
        "代码": None,
        "最新价": None,
        "量": None,
        "未平仓合约": None,
        "合约单位": None,
        "时间": None,
        "涨跌幅": None,
        "买价": None,
        "卖价": None
    }

def get_next_batch_images(batch_size=100):
    """获取下一批图片文件，按创建时间排序"""
    directory = Path("C:/bloomberg")
    if not directory.exists():
        print(f"目录不存在: {directory}")
        return []
    
    # 读取上次处理的最后一个文件
    last_processed_time = 0
    try:
        with open('last_processed.txt', 'r') as f:
            last_processed_time = float(f.read().strip())
        print(f"上次处理到: {datetime.fromtimestamp(last_processed_time).strftime('%Y-%m-%d %H:%M:%S')}")
    except FileNotFoundError:
        print("从头开始处理")
    
    # 获取所有未处理的文件
    files = []
    for file in directory.glob("*.png"):
        created_time = os.path.getctime(file)
        if created_time > last_processed_time:
            files.append((file, created_time))
    
    # 按创建时间排序并返回指定数量
    sorted_files = sorted(files, key=lambda x: x[1])
    return sorted_files[:batch_size]

def process_image(file_path):
    """处理单个图片"""
    try:
        with open(file_path, "rb") as file:
            file_object = client.files.create(
                file=file,
                purpose="file-extract"
            )
        
        file_content = client.files.content(file_id=file_object.id).text
        
        messages = [
            {
                "role": "system",
                "content": "你是 Kimi，由 Moonshot AI 提供的人工智能助手，你更擅长图片OCR结构化识别文字",
            },
            {
                "role": "system",
                "content": file_content,
            },
            {"role": "user", "content": "帮我识别出图片中的表格部分，按照按照行数据整理后给我。表格中字段的顺序按照： 代码、最新价、量、未平仓合约、合约单位、时间、涨跌幅、买价、卖价 key,value 的json 列表形式输出"},
        ]
        

        completion = client.chat.completions.create(
            model="moonshot-v1-32k",
            messages=messages,
            temperature=0.3,
        )
        
        response_content = completion.choices[0].message.content
        json_str = response_content.split('```json\n')[1].split('\n```')[0]
        return json.loads(json_str)
        
    except Exception as e:
        print(f"处理文件 {file_path} 时出错: {str(e)}")
        return None

def process_single_file(file_info):
    """处理单个文件并返回结果"""
    file_path, created_time = file_info
    print(f"\n处理文件: {file_path.name}")
    
    result = process_image(file_path)
    if result:
        # 添加文件信息
        for item in result:
            item['文件名'] = file_path.name
            item['创建时间'] = datetime.fromtimestamp(created_time).strftime('%Y-%m-%d %H:%M:%S')
        return result
    return []

def main():
    while True:  # 添加无限循环来处理所有批次
        # 获取下一批图片
        image_files = get_next_batch_images(100)
        if not image_files:
            print("没有找到新的图片文件，等待30秒后重试...")
            time.sleep(30)  # 等待30秒后继续检查新文件
            continue

        print(f"\n本批次找到 {len(image_files)} 个文件")
        
        # 读取现有的Excel文件（如果存在）
        existing_data = []
        excel_file = 'bloomberg_data.xlsx'
        if os.path.exists(excel_file):
            try:
                existing_df = pd.read_excel(excel_file)
                existing_data = existing_df.to_dict('records')
            except Exception as e:
                print(f"读取现有Excel文件时出错: {str(e)}")
        
        all_data = existing_data  # 使用现有数据作为基础
        
        # 使用线程池处理图片
        max_workers = min(10, 20)  # 限制最大线程数
        with ThreadPoolExecutor(max_workers=max_workers) as executor:
            # 提交所有任务
            future_to_file = {executor.submit(process_single_file, file_info): file_info 
                            for file_info in image_files}
            
            # 处理完成的任务
            for future in as_completed(future_to_file):
                file_info = future_to_file[future]
                try:
                    result = future.result()
                    if result:
                        all_data.extend(result)
                        
                        # 保存当前进度
                        with open('last_processed.txt', 'w') as f:
                            f.write(str(file_info[1]))
                        
                        # 追加保存到Excel
                        df = pd.DataFrame(all_data)
                        df.to_excel(excel_file, index=False)
                        
                        print(f"文件 {file_info[0].name} 处理完成")
                except Exception as e:
                    print(f"处理文件 {file_info[0].name} 时出错: {str(e)}")
                
                time.sleep(0.5)  # 降低API请求频率
        
        print(f"\n本批次处理完成，共处理 {len(all_data)} 条数据")

if __name__ == "__main__":
    main()
    