## 字符串的学习
print('hello "python"')

print("hello 'python'")

print('''
hello
python
      dfsd
'''
)

x = '1'
print(f"{x}")

## 2 成员运算
print(x in '123')

## 内置函数
## 1 出现次数
print("Simple is better than complex".count('better'))

## 2 分割
# print("a,b,c,d".split[','])