import requests  

# GitLab API token  
token = '1pW31P1GCLRTNv6mcoWy'  

# GitLab API URL  
url = 'http://172.16.50.115/api/v4'  

# Headers  
headers = {  
    'PRIVATE-TOKEN': token  
}  

# Send GET request  
response = requests.get(url, headers=headers)  

# Check response status  
if response.status_code == 200:  
    projects = response.json()  
    for project in projects:  
        print(f"Project ID: {project['id']}, Name: {project['name']}")  
else:  
    print(f"Failed to retrieve projects: {response.status_code}")