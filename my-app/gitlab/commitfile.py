import requests  
from datetime import datetime  

# GitLab 访问令牌和项目信息  
GITLAB_API_URL = 'http://172.16.50.115/api/v4'  
PRIVATE_TOKEN = '1pW31P1GCLRTNv6mcoWy'
PROJECT_ID = '196'  
BRANCH_NAME = 'master'

# 指定时间段  
START_DATE = '2024-10-01T00:00:00Z'  
END_DATE = '2024-10-10T23:59:59Z'  

def get_commits():  
    headers = {'PRIVATE-TOKEN': PRIVATE_TOKEN}  
    params = {  
        'ref_name': BRANCH_NAME,  
        'since': START_DATE,  
        'until': END_DATE  
    }  
    response = requests.get(f'{GITLAB_API_URL}/projects/{PROJECT_ID}/repository/commits', headers=headers, params=params)  
    response.raise_for_status()  
    return response.json()  

def get_files_in_commit(commit_id):  
    headers = {'PRIVATE-TOKEN': PRIVATE_TOKEN}  
    response = requests.get(f'{GITLAB_API_URL}/projects/{PROJECT_ID}/repository/commits/{commit_id}/diff', headers=headers)  
    response.raise_for_status()  
    return response.json()  

def main():  
    commits = get_commits()  
    new_files = []  

    for commit in commits:  
        commit_id = commit['id']  
        diffs = get_files_in_commit(commit_id)  
        
        for diff in diffs:  
            if diff['new_file']:  
                new_files.append(diff['new_path'])  

    print("New files created in the specified time period:")  
    for file in new_files:  
        print(file)  

if __name__ == '__main__':  
    main()