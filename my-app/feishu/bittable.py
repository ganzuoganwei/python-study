import requests  

# 获取访问令牌的函数  
def get_access_token(app_id, app_secret):  
    url = "https://open.feishu.cn/open-apis/auth/v3/tenant_access_token/internal/"  
    headers = {"Content-Type": "application/json"}  
    data = {  
        "app_id": app_id,  
        "app_secret": app_secret  
    }  
    response = requests.post(url, json=data, headers=headers)  
    return response.json().get("tenant_access_token")  

# 查询多维表格数据的函数  
def query_bitable_data(access_token, app_token, table_id, field1, value1, field2, value2):  
    url = f"https://open.feishu.cn/open-apis/bitable/v1/apps/{app_token}/tables/{table_id}/records/query"  
    headers = {  
        "Authorization": f"Bearer {access_token}",  
        "Content-Type": "application/json"  
    }  
    params = {  
        "filter": f"AND({{field1}}='{value1}', {{field2}}='{value2}')"  
    }  
    response = requests.post(url, headers=headers, json=params)  
    return response.json().get("data", {}).get("items", [])  

# 示例使用  
app_id = "cli_a780a2207f7b500e"  
app_secret = "oNj8JZm87E58FlHtkKjUtgkjuxlxngM0"  
app_token = "your_bitable_app_token"  
table_id = "your_table_id"  
field1 = "field1_name"  
field2 = "field2_name"  
value1 = "value_to_match_for_field1"  
value2 = "value_to_match_for_field2"  

access_token = get_access_token(app_id, app_secret)  
result = query_bitable_data(access_token, app_token, table_id, field1, value1, field2, value2)  

print("Filtered Data:", result)